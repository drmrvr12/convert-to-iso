﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpCompress;
using DiscUtils;
using DiscUtils.Iso9660;

namespace ConvertToISO {
    class Program {
        [STAThread]
        static void Main(string[] args) {
            if (args.Length == 0) {
                ShowHelp();
                Environment.Exit(0x00000000);
            } else {
                ProgramMode mode = ProgramMode.MODE_NONE;
                String input = String.Empty;
                String output = String.Empty;
                String label = String.Empty;

                if (args.Length == 1)
                    input = args[0];
                else
                    foreach (var X in args) {
                        switch (X.ToUpper()) {
                            case "/Q":
                                mode |= ProgramMode.MODE_QUIET;
                                break;

                            case "/S":
                                mode |= ProgramMode.MODE_ASAVE;
                                break;

                            case "/D":
                                mode |= ProgramMode.MODE_ISDIR;
                                break;

                            case "/?":
                                ShowHelp();
                                Environment.Exit(0x00000000);
                                return;

                            case string w when w.StartsWith("/I:"):
                                input = GetStringFromArgument(X);
                                break;

                            case string w when w.StartsWith("/O:"):
                                output = GetStringFromArgument(X);
                                break;
                            case string w when w.StartsWith("/L:"):
                                label = GetStringFromArgument(X);
                                break;
                            default:
                                Console.WriteLine("Invalid argument: \"{0}\".", X);
                                Exit(0x00000001);
                                break;
                        }
                    }
                const int TOTAL_STEPS = 4;
                Console.WriteLine("\r\n[STEP 1/{0}] PREREQUISITES\r\n", TOTAL_STEPS);

                IArchiveExtractor ae = new GeneralExtractor();
                string tmp = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "$CONVTISO_" + new Random().Next(100000000, 999999999));
                DateTime Start = DateTime.Now;

                System.IO.Directory.CreateDirectory(tmp);

                ae.Source = input;
                ae.TargetPath = tmp;

                if (string.IsNullOrEmpty(input)) {
                    Console.WriteLine("  [{0}] INPUT is not specified. The operation cannot continue.", DateTime.Now);
                    Exit(0x000000FF);
                }

                if (mode.HasFlag(ProgramMode.MODE_ASAVE) && mode.HasFlag(ProgramMode.MODE_QUIET)) {
                    Console.WriteLine("  [{0}] Conflicting flags: both /Q and /S are specified. Please specify only one. The operation cannot continue.", DateTime.Now);
                    Exit(0x0000000C);
                }

                if (mode.HasFlag(ProgramMode.MODE_ASAVE)) {
                    Console.WriteLine("  [{0}] /S is set. Please select a new output path.", DateTime.Now);
                    output = GetNewPath();
                }

                if (string.IsNullOrEmpty(output)) {
                    output = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(input), System.IO.Path.GetFileNameWithoutExtension(input) + ".ISO");
                    Console.WriteLine("  [{0}] Output not specified, defaulting to {1}.", DateTime.Now, output);
                }

                if (mode.HasFlag(ProgramMode.MODE_ISDIR)) {
                    Console.WriteLine("  [{0}] Input is a directory, skipping step 1.", DateTime.Now);
                    tmp = input;
                } else {
                    Console.WriteLine("\r\n[STEP 2/{0}] EXTRACT TO TEMP DIRECTORY\r\n", TOTAL_STEPS);

                    ae.Failed += (object sender, ExtractionFailedEventArgs e) => {
                        Console.WriteLine("Fatal error - extraction failed with message " + e.Reason + "\r\n\r\n The operation cannot continue.");

                        if (!mode.HasFlag(ProgramMode.MODE_ISDIR)) {
                            Start = DateTime.Now;
                            Console.WriteLine("  [{0}] Removing non-empty directory \"{1}\".", DateTime.Now, tmp);
                            System.IO.Directory.Delete(tmp, true);
                        } else {
                            Console.WriteLine("  [{0}] \"{1}\" is not a temp directory and will not be deleted.", DateTime.Now, tmp);
                        }

                        Exit(0x0000000A);
                    };

                    ae.Load();

                    Console.WriteLine("  [{0}] Detected archive type: " + ae.GetArchiveType(), DateTime.Now);
                    int size = ae.GetTargetFilesize();
                    Console.WriteLine("  [{0}] Total target size: " + (size != -1 ? size + "" : "UNKNOWN"), DateTime.Now);



                    Start = DateTime.Now;
                    Console.WriteLine("  [{1}] Extracting to \"{0}\", please wait...", tmp, DateTime.Now);

                    ae.Extract();

                    ae.Dispose();

                    Console.WriteLine("  [{0}] Done in {1}.", DateTime.Now, (DateTime.Now - Start).ToString("G"));
                }

                Console.WriteLine("\r\n[STEP 3/{0}] BUILD THE ISO IMAGE\r\n", TOTAL_STEPS);

                Console.WriteLine("  [{0}] Initializing the CD Builder.", DateTime.Now);

                CDBuilder builder = new CDBuilder();
                builder.UseJoliet = true;
                if (!String.IsNullOrEmpty(label))
                    builder.VolumeIdentifier = label;
                Console.WriteLine("  [{0}] Adding the files...", DateTime.Now);

                var fsen = System.IO.Directory.GetFiles(tmp, "*.*", System.IO.SearchOption.AllDirectories);
                for (int I = 0; I < fsen.Length; I++) {
                    var X = fsen[I];
                    bool isDir = (System.IO.File.GetAttributes(X) & System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory;
                    Console.Write("\r  [{0}] {1}/{2} files added.", DateTime.Now, I + 1, fsen.Length);
                    if (isDir) continue;
                    builder.AddFile(X.Replace(tmp, ""), X);
                    
                }
                Console.WriteLine();
                Start = DateTime.Now;
                Console.WriteLine("  [{0}] Compiling.", DateTime.Now);

                

                try {
                    if (System.IO.File.Exists(output)) {
                        if (mode.HasFlag(ProgramMode.MODE_QUIET)) {
                            Console.WriteLine("  [{0}] \"{1}\" already exists, deleting due to quiet mode.", DateTime.Now, output);
                            System.IO.File.Delete(output);
                        } else {
                            Console.WriteLine("  [{0}] \"{1}\" already exists, please supply a new output path.", DateTime.Now, output);
                            output = GetNewPath();
                        }

                    }
                    System.IO.File.WriteAllLines(output, new[] { "THIS", "IS", "A", "TEST" });
                    System.IO.File.Delete(output);
                } catch (Exception) {
                    Console.WriteLine("  [{0}] Access denied, please supply a new output path.", DateTime.Now);
                    output = GetNewPath();
                }

                builder.Build(output);
                Console.WriteLine("  [{0}] Done in {1}.", DateTime.Now, (DateTime.Now - Start).ToString("G"));


                Console.WriteLine("\r\n[STEP 4/{0}] CLEANUP\r\n", TOTAL_STEPS);

                if (!mode.HasFlag(ProgramMode.MODE_ISDIR)) {
                    Start = DateTime.Now;
                    Console.WriteLine("  [{0}] Removing non-empty directory \"{1}\".", DateTime.Now, tmp);
                    System.IO.Directory.Delete(tmp, true);
                } else {
                    Console.WriteLine("  [{0}] \"{1}\" is not a temp directory and will not be deleted.", DateTime.Now, tmp);
                }

                Console.WriteLine("  [{0}] Done in {1}.", DateTime.Now, (DateTime.Now - Start).ToString("G"));
                Console.WriteLine("  [{0}] Freeing unmanaged resources.", DateTime.Now);
                Console.WriteLine("  [{0}] Nothing to dispose.", DateTime.Now);
                Console.WriteLine("  [{0}] Build succesful, exiting.", DateTime.Now);
                Exit(0x00000000);
            }
        }
        static string GetStringFromArgument(string w) => w.Substring(w.IndexOf(':') + 1).Trim();

        static string GetNewPath() {
            System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog();
            // sf.ShowHelp = true;
            sf.Filter = "ISO Images (*.ISO)|*.ISO";
            var dr = sf.ShowDialog(null);
            switch (dr) {
                case System.Windows.Forms.DialogResult.OK:
                    return sf.FileName;
                default:
                    Console.WriteLine("  [{0}] Failed to acquire a new output path, exiting.", DateTime.Now);
                    Exit(0x0000000B);
                    return null;
            }
        }

        static void Exit(int code) {
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            Environment.Exit(code);
        }

        [Flags]
        enum ProgramMode {
            MODE_NONE = 0,
            MODE_QUIET = 1,
            MODE_ASAVE = 2,
            MODE_ISDIR = 4,
        }

        static void ShowHelp() {
            Console.WriteLine("Usage:\r\n\r\n" +
    "  CONVTISO [/Q] [/S] [/D] [/I:INPUT] [/O:OUTPUT] [/L:LABEL] [/?]\r\n" +
    "  CONVTISO [INPUT]\r\n\r\n" +
    "/Q - Save the output in same directory as input; Display a Save dialog if permission is denied, overwrite if exists.\r\n" +
    "/S - Always show the Save dialog.\r\n" +
    "/D - Input is a directory.\r\n" +
    "/? - Display this text.\r\n" +
    "/L:LABEL - Disk label.\r\n" +
    "/I:INPUT - Input archive / directory.\r\n" +
    "/O:OUTPUT - Output filename.\r\n\r\n" +
    "Note:\r\n\r\n" +
    "  If no argument except INPUT is specified, the command line is treated as CONVTISO /I:INPUT\r\n");
        }
    }
}
