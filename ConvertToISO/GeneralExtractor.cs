﻿using SharpCompress.Archives;
using SharpCompress.Common;
using SharpCompress.Readers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertToISO {
    class GeneralExtractor : IArchiveExtractor, IDisposable {
        public String TargetPath { internal get; set; }

        public String Source { internal get; set; }

        public event EventHandler<ExtractionFailedEventArgs> Failed;

        public void Extract() {
            try {
                r.ExtractAllEntries().WriteAllToDirectory(TargetPath, new ExtractionOptions() {
                    ExtractFullPath = true,
                    Overwrite = true,
                    PreserveFileTime = true,
                    // PreserveAttributes = true

                });
            } catch (Exception x) {
                Failed?.Invoke(this, new ExtractionFailedEventArgs(x.ToString()));
            }
            
        }
        private Stream s = null;
        private IArchive r = null;
        public Int32 GetTargetFilesize() => (int)r.TotalUncompressSize;
        public void Load() {
            try {
                s = File.OpenRead(Source);
                r = ArchiveFactory.Open(s);
            } catch (Exception x) {
                Failed?.Invoke(this, new ExtractionFailedEventArgs(x.ToString()));
            }
        }
        public String GetArchiveType() => r.Type.ToString();

        public void Dispose() {
            s.Dispose();
            r.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
