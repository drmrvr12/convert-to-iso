﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertToISO {
    public class ExtractionFailedEventArgs : EventArgs {
        protected ExtractionFailedEventArgs() { }
        public ExtractionFailedEventArgs(string Reason) {
            this.Reason = Reason;
        }
        public string Reason { get; set; }
    }
}
