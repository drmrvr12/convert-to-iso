﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertToISO {
    public interface IArchiveExtractor : IDisposable {
        event EventHandler<ExtractionFailedEventArgs> Failed;
        string TargetPath { set; }
        string Source { set; }

        string GetArchiveType();
        void Extract();
        void Load();
        int GetTargetFilesize();
    }
}
