﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;

/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENCE
                    Version 3.1, July 2019

 by Sam Hocevar <sam@hocevar.net>
    theiostream <matoe@matoe.co.cc>
    dtf         <wtfpl@dtf.wtf>

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENCE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
     */

namespace AddShellExtension {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "CONVTISO.EXE|CONVTISO.EXE";
            DialogResult dr = of.ShowDialog();
            switch (dr) {
                case DialogResult.OK:
                    Form D = new System.Windows.Forms.Form();
                    D.ShowInTaskbar = false;
                    D.Opacity = .0001F;
                    D.Show();
                    D.TopMost = true;
                    DialogResult dr2 = MessageBox.Show(D, "Are you sure you want to add \"Convert to ISO...\" to the \"Directory\" and \"*\" \\shell registry keys?", "Shell Extension", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    switch (dr2) {
                        case DialogResult.Yes:
                            try {
                                RegistryKey k = Registry.ClassesRoot.CreateSubKey(@"*\shell\CreateISOShell");
                                k.SetValue("", "Convert to ISO...");
                                RegistryKey c = Registry.ClassesRoot.CreateSubKey(@"*\shell\CreateISOShell\command");
                                c.SetValue("", "\"" + of.FileName + "\" \"%1\"");

                                RegistryKey d = Registry.ClassesRoot.CreateSubKey(@"Directory\shell\CreateISOShell");
                                d.SetValue("", "Create ISO...");
                                RegistryKey v = Registry.ClassesRoot.CreateSubKey(@"Directory\shell\CreateISOShell\command");
                                v.SetValue("", "\"" + of.FileName + "\" /I:\"%1\" /D");

                            } catch (Exception x) {
                                MessageBox.Show(x + "");
                            }
                            
                            MessageBox.Show(D, "The values have been succesfully added to the registry.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            D.Close();
                            Environment.Exit(0x00000000);
                            break;
                        default:
                            D.Close();
                            Environment.Exit(0x00000002);
                            break;
                    }

                    break;
                default:
                    Environment.Exit(0x00000001);
                    break;
            }
        }
    }
}
